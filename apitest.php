<?php
/**
 * Plugin Name: Api Test
 */

add_action('init', 'videos');
function videos() {
	$args =	array(
			'labels' => array(
				'name' => __('Videos'),
				'singular_name' => __('Video'),
			),
			'public' => true,
			'show_in_rest' => true,
			'has_archive' => true,
			'publicly_queryable' => true,
			'supports' => array('title'),
			'rewrite' => array( 'slug' => 'videos' ),
		);
	register_post_type( 'videos', $args );
}

$object_type = 'post';
$meta_args = array(
	'type' => 'string',
	'description' => 'video id',
	'single' => true,
	'show_in_rest' => true,
);
register_meta( 'posts','videoid', $meta_args);

add_action( 'add_meta_boxes' , 'add_videoid' ); 	
function add_videoid() {
	$screens = ['videos', 'page'];
	foreach($screens as $screen) {
		add_meta_box(
			'video_id', //unique ID
			'Video ID', //Title
			'video_html', // callback
			$screen // post type
		);
	}

}

function video_html($post)
{
    ?>
    <label for="videoid">video id</label>
	<input type="text" name="videoid" 
		value="<?php echo get_post_meta($post->ID, "videoid", true); ?>">
    <?php
}

add_action('save_post', 'save_video', 10, 3);
function save_video($post_id, $post, $update) {
	$screens = ['videos', 'page'];
	foreach($screens as $screen) {

		$videoid = "";
		if(isset($_POST["videoid"])) {
			$videoid = $_POST["videoid"];
		}
		update_post_meta($post_id, "videoid", $videoid);
	}

}